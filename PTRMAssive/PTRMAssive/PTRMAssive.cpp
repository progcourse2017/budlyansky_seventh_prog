#include "stdafx.h"
#include <iostream>

using namespace std;

void sortArray(int *array, int size);



int main() 
{
	int array[5] = {10,9,87,6,1};

	int * ptrArray = array;

	sortArray(ptrArray, 5);

	for(int i=0;i<5;i++)
	{
		cout <<ptrArray[i] <<"\n";
	}

	system("pause");

	return 0;

}

void sortArray(int *array, int size) 
{
	int tempSwap=0;

	bool itSwap = false;

	while (!itSwap)
	{
		itSwap = true;

		for (int i = 0; i < (size - 1); i++)
		{
			if (array[i] > array[i + 1])
			{
				tempSwap = array[i];
				array[i] = array[i + 1];
				array[i + 1] = tempSwap;
				itSwap = false;
			}

		}
	}

};
